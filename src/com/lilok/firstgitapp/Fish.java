package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class Fish {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private int size;
    private int speed;
    private boolean predator;
    private int khaviar;
    private int sweemingKmPy;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setPredator(boolean predator) {
        this.predator = predator;
    }

    public void setKhaviar(int khaviar) {
        this.khaviar = khaviar;
    }

    public void setSweemingKmPy(int sweemingKmPy) {
        this.sweemingKmPy = sweemingKmPy;
    }

    public static String moveTo(float x, float y) {
        float currentX = 0;
        float currentY = 0;
        return (Math.sqrt((x + currentX) * (x + currentX) + (y - currentY) * (y - currentY)) + " Fish moved to the given distance!");

    }


    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(Integer.parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "size":
                this.setSize(Integer.parseInt(value));
                break;
            case "speed":
                this.setSpeed(Integer.parseInt(value));
                break;
            case "predator":
                this.setPredator(Boolean.parseBoolean(value));
                break;
            case "khaviar":
                this.setKhaviar(Integer.parseInt(value));
                break;
            case "sweemingKmPy":
                this.setSweemingKmPy(Integer.parseInt(value));
                break;
        }
    }

    public static class builder {
        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private int size;
        private int speed;
        private boolean predator;
        private int khaviar;
        private int sweemingKmPy;
    }

    public Fish build() {
        Fish fish = new Fish();

        fish.name = this.name;
        fish.chromosomeCount = this.chromosomeCount;
        fish.kingdom = this.kingdom;
        fish.type = this.type;
        fish.size = this.size;
        fish.speed = this.speed;
        fish.predator = this.predator;
        fish.khaviar = this.khaviar;
        fish.sweemingKmPy = this.sweemingKmPy;

        return fish;
    }


}
