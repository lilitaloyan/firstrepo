package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class BacteriaMushroom {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private int size;
    private boolean poisonous;
    private String usedIn;
    private int growSpeed;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }

    public void setUsedIn(String usedIn) {
        this.usedIn = usedIn;
    }

    public void setGrowSpeed(int growSpeed) {
        this.growSpeed = growSpeed;
    }

    public static class Builder {
        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private int size;
        private boolean poisonous;
        private String usedIn;
        private int growSpeed;

    }

    public BacteriaMushroom build() {

        BacteriaMushroom bacteriaMushroom = new BacteriaMushroom();

        bacteriaMushroom.name = this.name;
        bacteriaMushroom.chromosomeCount = this.chromosomeCount;
        bacteriaMushroom.kingdom = this.kingdom;
        bacteriaMushroom.type = this.type;
        bacteriaMushroom.size = this.size;
        bacteriaMushroom.poisonous = this.poisonous;
        bacteriaMushroom.usedIn = this.usedIn;
        bacteriaMushroom.growSpeed = this.growSpeed;

        return bacteriaMushroom;
    }
}
