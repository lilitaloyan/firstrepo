package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class WoodMushroom {
    private String name;
    private int chromosome_count;
    private String kingdom;
    private String type;
    private int size;
    private boolean poisonous;
    private int head_radius;
    private boolean edible;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosome_count(int chromosome_count) {
        this.chromosome_count = chromosome_count;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }

    public void setHead_radius(int head_radius) {
        this.head_radius = head_radius;
    }

    public void setEdible(boolean edible) {
        this.edible = edible;
    }

    public static class Builder {

        private String name;
        private int chromosome_count;
        private String kingdom;
        private String type;
        private int size;
        private boolean poisonous;
        private int head_radius;
        private boolean edible;
    }

    public WoodMushroom build() {
        WoodMushroom wood_mushroom = new WoodMushroom();

        wood_mushroom.name = this.name;
        wood_mushroom.chromosome_count = this.chromosome_count;
        wood_mushroom.kingdom = this.kingdom;
        wood_mushroom.type = this.type;
        wood_mushroom.size = this.size;
        wood_mushroom.poisonous = this.poisonous;
        wood_mushroom.head_radius = this.head_radius;
        wood_mushroom.edible = this.edible;

        return wood_mushroom;
    }
}
