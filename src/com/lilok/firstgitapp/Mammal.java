package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class Mammal {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private int size;
    private int speed;
    private boolean predator;
    private int mpc;
    private int legCount;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setPredator(boolean predator) {
        this.predator = predator;
    }

    public void setMpc(int mpc) {
        this.mpc = mpc;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }


    public static String moveTo(float x, float y) {
        float currentX = 0;
        float currentY = 0;
        return (Math.sqrt((x + currentX) * (x + currentX) + (y - currentY) * (y - currentY)) + " Bird moved to the given distance!");

    }

    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(Integer.parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "size":
                this.setSize(Integer.parseInt(value));
                break;
            case "speed":
                this.setSpeed(Integer.parseInt(value));
                break;
            case "predator":
                this.setPredator(Boolean.parseBoolean(value));
                break;
            case "mpc":
                this.setMpc(Integer.parseInt(value));
                break;
            case "legCount":
                this.setLegCount(Integer.parseInt(value));
        }
    }


    public static class Builder {
        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private int size;
        private int speed;
        private boolean predator;
        private int mpc;
        private int legCount;

    }

    public Mammal build() {
        Mammal mammal = new Mammal();
        mammal.name = this.name;
        mammal.chromosomeCount = this.chromosomeCount;
        mammal.kingdom = this.kingdom;
        mammal.type = this.type;
        mammal.size = this.size;
        mammal.speed = this.speed;
        mammal.predator = this.predator;
        mammal.mpc = this.mpc;
        mammal.legCount = this.legCount;

        return mammal;
    }
}
