package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class Grass {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private boolean decorative;
    private int oph;
    private int size;
    private int isWeed;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDecorative(boolean decorative) {
        this.decorative = decorative;
    }

    public void setOph(int oph) {
        this.oph = oph;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setIsWeed(int isWeed) {
        this.isWeed = isWeed;
    }


    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(Integer.parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "decorative":
                this.setDecorative(Boolean.parseBoolean(value));
                break;
            case "oph":
                this.setOph(Integer.parseInt(value));
                break;
            case "size":
                this.setSize(Integer.parseInt(value));
                break;
            case "isWeed":
                this.setIsWeed(Integer.parseInt(value));
                break;
        }
    }

    public static class Builder {
        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private boolean decorative;
        private int oph;
        private int size;
        private int isWeed;
    }

    public Grass build() {
        Grass grass = new Grass();

        grass.name = this.name;
        grass.chromosomeCount = this.chromosomeCount;
        grass.kingdom = this.kingdom;
        grass.type = this.type;
        grass.decorative = this.decorative;
        grass.oph = this.oph;
        grass.size = this.size;
        grass.isWeed = this.isWeed;

        return grass;
    }
}
