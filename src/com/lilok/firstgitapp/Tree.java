package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class Tree {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private boolean decorative;
    private int oph;
    private int size;
    private int lifetime;
    private int woodStrengh;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDecorative(boolean decorative) {
        this.decorative = decorative;
    }

    public void setOph(int oph) {
        this.oph = oph;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setWoodStrengh(int woodStrengh) {
        this.woodStrengh = woodStrengh;
    }

    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(Integer.parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "decorative":
                this.setDecorative(Boolean.parseBoolean(value));
                break;
            case "oph":
                this.setOph(Integer.parseInt(value));
                break;
            case "size":
                this.setSize(Integer.parseInt(value));
                break;
            case "lifetime":
                this.setLifetime(Integer.parseInt(value));
                break;
            case "woodStrengh":
                this.setWoodStrengh(Integer.parseInt(value));
        }
    }


    public static class builder {
        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private boolean decorative;
        private int oph;
        private int size;
        private int lifetime;
        private int woodStrengh;

    }

    public Tree build() {
        Tree tree = new Tree();

        tree.name = this.name;
        tree.chromosomeCount = this.chromosomeCount;
        tree.kingdom = this.kingdom;
        tree.type = this.type;
        tree.decorative = this.decorative;
        tree.oph = this.oph;
        tree.size = this.size;
        tree.lifetime = this.lifetime;
        tree.woodStrengh = this.woodStrengh;

        return tree;
    }
}
