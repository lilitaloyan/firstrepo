package com.lilok.firstgitapp;



import static java.lang.Integer.parseInt;

/**
 * Created by Lilok on 1/17/2018.
 */
public class Bird {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private int size;
    private int speed;
    private boolean predator;
    private int wingSize;
    private int[] colors;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setPredator(boolean predator) {
        this.predator = predator;
    }

    public void setWingSize(int wingSize) {
        this.wingSize = wingSize;
    }

    public void setColors(int[] colors) {
        this.colors = colors;
    }


    public static String moveTo(float x, float y) {
        float currentX = 0;
        float currentY = 0;
        return (Math.sqrt((x + currentX) * (x + currentX) + (y - currentY) * (y - currentY)) + " Bird moved to the given distance!");

    }

 /*   public static int[] toArray(String colorInt) {
        Integer g;
        int[] h;
        String[] v;
        v = colorInt.split("\\s+");
        for (int i = 0; v.length > i; i++) {
            g = parseInt(v[i]);

        }

    }*/


    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "size":
                this.setSize(parseInt(value));
                break;
            case "speed":
                this.setSpeed(parseInt(value));
                break;
            case "predator":
                this.setPredator(Boolean.parseBoolean(value));
                break;
            case "wingSize":
                this.setWingSize(parseInt(value));
                break;
          /*  case "colors":
                this.setColors();*/
        }
    }

    public static class Builder {
        private String name;
        private int chromosomеCount;
        private String kingdom;
        private String type;
        private int size;
        private int speed;
        private boolean predator;
        private int wingSize;
        private int[] colors;

        public Bird build() {
            Bird bird = new Bird();
            bird.name = this.name;
            bird.chromosomeCount = this.chromosomеCount;
            bird.kingdom = this.kingdom;
            bird.type = this.type;
            bird.size = this.size;
            bird.speed = this.speed;
            bird.predator = this.predator;
            bird.wingSize = this.wingSize;
            bird.colors = this.colors;

            return bird;
        }
    }
}



