package com.lilok.firstgitapp;

/**
 * Created by Lilok on 1/23/2018.
 */
public class Bush {
    private String name;
    private int chromosomeCount;
    private String kingdom;
    private String type;
    private boolean decorative;
    private int oph;
    private int size;
    private int bps;

    public void setName(String name) {
        this.name = name;
    }

    public void setChromosomeCount(int chromosomeCount) {
        this.chromosomeCount = chromosomeCount;
    }

    public void setKingdom(String kingdom) {
        this.kingdom = kingdom;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDecorative(boolean decorative) {
        this.decorative = decorative;
    }

    public void setOph(int oph) {
        this.oph = oph;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setBps(int bps) {
        this.bps = bps;
    }


    public void setFild(String fildName, String value) {
        switch (fildName) {
            case "name":
                this.setName(value);
                break;
            case "chromosomeCount":
                this.setChromosomeCount(Integer.parseInt(value));
                break;
            case "kingdom":
                this.setKingdom(value);
                break;
            case "type":
                this.setType(value);
                break;
            case "decorative":
                this.setDecorative(Boolean.parseBoolean(value));
                break;
            case "oph":
                this.setOph(Integer.parseInt(value));
                break;
            case "size":
                this.setSize(Integer.parseInt(value));
                break;
            case "bps":
                this.setBps(Integer.parseInt(value));
                break;

        }
    }

    public static class builer {

        private String name;
        private int chromosomeCount;
        private String kingdom;
        private String type;
        private boolean decorative;
        private int oph;
        private int size;
        private int bps;
    }

    public Bush build() {
        Bush bush = new Bush();
        bush.name = this.name;
        bush.chromosomeCount = this.chromosomeCount;
        bush.kingdom = this.kingdom;
        bush.type = this.type;
        bush.decorative = this.decorative;
        bush.oph = this.oph;
        bush.size = this.size;
        bush.bps = this.bps;

        return bush;
    }
}
