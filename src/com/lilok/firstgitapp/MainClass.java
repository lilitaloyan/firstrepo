package com.lilok.firstgitapp;

import javafx.scene.shape.MoveTo;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Lilok on 11/16/2017.
 * /**
 * patkeracreq vor inchvor xaxi mej linelu en araracner. nkarnerov sirun, animacianerov.
 * <p>
 * bolor araracner@ unen:
 * name: String
 * chromosome_count: int
 * kingdom: String
 * type: String
 * size: int
 * <p>
 * kingdom@ linum a: "animal", "plant", "mushroom"
 * amen kingdom uni ira typer@. orinak animalner@ linum en bird, fish, mammal (katnasun)
 * planter@ iranc typer@ unen, mushroomner@` iranc
 * <p>
 * bolor animalner@ unen:
 * predator: boolean
 * speed: int
 * <p>
 * bolor planter@ unen:
 * decorative: boolean
 * oph: int (oxygen per hour, karevor chi)
 * <p>
 * bolor snker@ unen:
 * poisonous: boolean
 * <p>
 * animalneric birder@ unen:
 * wing_size: int
 * colors: [int]
 * <p>
 * mammalner@ unen:
 * mpc: int (chgitem inch a, Lilitin harcreq :D)
 * leg_count: int
 * <p>
 * fisher@ unen:
 * "khaviar": int
 * "sweeming_km_py": int
 * <p>
 * u senc sharunak nayeq eli vor planter@ inch unen, vor mushroomner@ inch unen.
 * mi hat karevor moment. bacteria_mushroom-ner@ unen used_in, bayc karoxa jsoni mej ed arjeq@ drac chlini. ushadir klineq.
 * <p>
 * Yev ayspisov task@:
 * es json@ mi hat file-i mej qceq. u sarqeq cragir, vor@ kardum a, es sax araracnerin pahum a inchvor dzev,
 * vor heto karoxanam asel tur indz bolor mushroomner@, inq@ ta.
 * baci ed:
 * bolor araracner@ petqa unenan method: animate. vor xaxi mej animacia linen kangnac texum. petqa karoxanam inchvor
 * dzev asem saxin vor sksen animate linel.
 * ed animate funkciayi mej kgreq System.out.println(name + ": animating"); enqan vor inchvor ban katarvi.
 * entadreq vor hetagayum ed system out-i tex@ petqa ekrani vra animate lini ed ararac@.
 * bolor kendaniner@ petqa unenan moveTo(fl oat x, float y) method. mej@ eli havai system out
 * kgreq vor ha, move a exel ed qo tvac  ket@. u eli entadrenq vor hetagayum dra tex@ ekrani vra sirun kendanin gnalu a @dtex.
 * bolor mushroomner@ petqa unenan grow() method. eli nuyn tramabanutyamb. bolor plantnern
 * el petqa unean breath() method. eli nuyn dzeverov.
 * uremn sarqelu eq 8 hat class:
 * Bird
 * Fish
 * Mammal
 * Tree
 * Bus*h
 * Grass
 * WoodMushroom
 * BacteriaMushroom
 * <p>
 * u jsoni mejinner@ es classeri objectner sarqeq. asenq kardaciq mi hat, tesaq animal a, tesaq bird a,
 * uremn new Bird() petqa sarqeq, u ira xarakteristikeq@ mej@ dneq.
 */

public class MainClass {
    public static void main(String[] args) throws JSONException {

        File file = new File("D:\\json.txt");
        JSONArray myJson = FileReader.readFile(file);


        String v = Bird.moveTo(1, 4);
        //  System.out.print(myJson);
        ArrayList<String> arr = new ArrayList<>();

      /*  for (int i = 0; myJson.length() > i; i++) {

            arr.add(myJson.getJSONObject(i).toString());

        }*/

        System.out.print(myJson);
    }


    /**
     * x
     * Created by Lilok on 1/25/2018.
     */
    public static class FileReader {

        public static JSONArray readFile(File file) throws JSONException {
            if (file == null || !file.exists()) {
                return null;
            }
            StringBuilder line = new StringBuilder();
            BufferedReader reader;

            try {
                reader = new BufferedReader(new java.io.FileReader(file));

                while (reader.ready()) {
                    line.append(reader.readLine());
                    line.append(System.getProperty("line.separator"));
                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            JSONArray json = new JSONArray(line.toString());
            return json;
        }
    }


    /**
     * Created by Lilok on 1/24/2018.
     */
    public static class Key2D {


        private final String outer;
        private final String inner;

        public Key2D(String outer, String inner) {
            this.outer = outer;
            this.inner = inner;
        }

    }

}

